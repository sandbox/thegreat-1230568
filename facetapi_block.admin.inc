<?php

/**
 * Settings form for rearranging the order of facet blocks within the container
 * block.
 */
function facetapi_block_settings_form($form, &$form_state) {
  $form = array();
  $include_current_search = variable_get('facetapi_block_current_search', FALSE);
  $form['current_search'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include the Current Search block'),
    '#default_value' => $include_current_search,
  );
  $form['facet_blocks'] = array(
    '#tree' => TRUE,
    '#theme' => 'facetapi_block_facet_order',
  );
  $facet_weights = variable_get('facetapi_block_facet_weights', array());
  module_load_include('inc', 'facetapi', 'facetapi.block');
  $facet_blocks = facetapi_get_block_info('block');
  $delta_map = facetapi_get_delta_map();

  $weight = array();
  foreach ($facet_blocks as $facet_delta => $facet_info) {
    $facet_name = $delta_map[$facet_delta];
    $weight[$facet_delta] = isset($facet_weights[$facet_name]) ? $facet_weights[$facet_name] : 0;
  }
  array_multisort($weight, SORT_ASC, $facet_blocks);
  $i = 0;
  foreach ($facet_blocks as $facet_delta => $facet_info) {
    if (!$include_current_search && strpos($delta_map[$facet_delta], 'current_search')) {
      // Exlude the current search block.
      continue;
    }
    $facet_name = $delta_map[$facet_delta];
    $facet_form = array();
    $facet_form['weight'] = array(
      '#type' => 'weight',
      '#default_value' => isset($facet_weights[$facet_name]) ? $facet_weights[$facet_name] : 0,
      '#delta' => 100,
    );
    $facet_form['title'] = array(
      '#type' => 'markup',
      '#markup' => $facet_name,
    );
    $facet_form['name'] = array(
      '#type' => 'hidden',
      '#value' => $facet_name,
    );
    $form['facet_blocks'][$i] = $facet_form;
    $i++;
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Theme the form elements for the facets as draggable table rows.
 *
 * @ingroup themeable
 */
function theme_facetapi_block_facet_order($variables) {
  $facet_blocks = $variables['facet_blocks'];

  drupal_add_tabledrag('facetapi-block-facet-table', 'order', 'sibling', 'facetapi-block-facet-weight');
  $rows = array();
  $header = array(
    t('Facet name'),
    t('Facet weight'),
  );
  
  foreach (element_children($facet_blocks) as $key) {
    $facet = &$facet_blocks[$key];
    $facet['weight']['#attributes']['class'] = array('facetapi-block-facet-weight');

    $facet_fields = array(
      array('data' => drupal_render($facet['title']), 'class' => array('facet-title')),
      array('data' => drupal_render($facet['weight']), 'class' => array('facet-weight')),
    );
  
    // Build the table row.
    $row = array(
      'data' => $facet_fields,
      'class' => array('draggable'),
    );
  
    // Add additional attributes to the row, such as a class for this row.
    if (isset($facet['#attributes'])) {
      $row = array_merge($row, $facet['#attributes']);
    }
    $rows[] = $row;
  }
  
  $build['facetapi_block_facets'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array('id' => 'facetapi-block-facet-table'),
    '#weight' => -1,
  );

  $output = drupal_render($build);
  return $output;
}

function facetapi_block_settings_form_submit(&$form, &$form_state) {
  // Invalidate the facet weight cache.
  cache_set('facetapi_block_weighted_facets', NULL);

  variable_set('facetapi_block_current_search', $form_state['values']['current_search']);

  $facet_weights = array();
  foreach($form_state['values']['facet_blocks'] as $facet) {
    $facet_weights[$facet['name']] = $facet['weight'];
  }
  variable_set('facetapi_block_facet_weights', $facet_weights);
  drupal_set_message(t('Facet weights have been saved.'));
}
